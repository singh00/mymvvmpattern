package com.example.mvvmex

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.mvvmex.databinding.ActivityMainBinding
import com.example.mvvmex.factory.UserDetailsFactory
import com.example.mvvmex.network.ApiInterface
import com.example.mvvmex.repository.UserRepository
import com.example.mvvmex.viewmodel.UserDetailsViewModel

class MainActivity : AppCompatActivity() {
    lateinit var binding:ActivityMainBinding
    lateinit var viewModel:UserDetailsViewModel



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding=DataBindingUtil.setContentView(this,R.layout.activity_main)

        val repository=ApiInterface.getInstance()

         viewModel=ViewModelProvider(this,UserDetailsFactory
            (UserRepository(repository,this),this)).get(UserDetailsViewModel::class.java)
        
        binding.userview=viewModel
        viewModel.getDataObserver().observe(this, Observer {
            if (it !=null){
                viewModel.setUserAdapter(it)
            }
        })
        viewModel.getDetailsApi()
    }
}