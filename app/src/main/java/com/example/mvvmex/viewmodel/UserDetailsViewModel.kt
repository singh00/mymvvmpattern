package com.example.mvvmex.viewmodel

import android.content.Context
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.mvvmex.adapter.UserAdapter
import com.example.mvvmex.reaponsemodel.UserResponseModel
import com.example.mvvmex.repository.UserRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.Call
import retrofit2.Response
import javax.security.auth.callback.Callback

class UserDetailsViewModel(private val repository: UserRepository, private val context: Context) : ViewModel() {

    lateinit var uData:MutableLiveData<UserResponseModel>
    lateinit var adapter:UserAdapter
    lateinit var mUserdata:ArrayList<UserResponseModel.UserResponseModelItem>

    init {
        uData= MutableLiveData()
        adapter= UserAdapter()
        mUserdata= ArrayList()
    }

    fun getUserAdapter():UserAdapter{
        return adapter
    }

    fun setUserAdapter(uData:ArrayList<UserResponseModel.UserResponseModelItem>){
        adapter.setDataList(uData)
        adapter.notifyDataSetChanged()
    }

    fun getDataObserver():MutableLiveData<UserResponseModel>{
        return uData
    }

    fun getDetailsApi(){
        viewModelScope.launch ( Dispatchers.IO){
            val response=repository.getUserDetails()
            response.enqueue(object : Callback,
                retrofit2.Callback<UserResponseModel> {
                override fun onResponse(
                    call: Call<UserResponseModel>,
                    response: Response<UserResponseModel>
                ) {
                    if (response.isSuccessful) {
                           uData.postValue(response.body())
                    } else {
                        uData.postValue(null)
                    }
                }

                override fun onFailure(call: Call<UserResponseModel>, t: Throwable) {
                    uData.postValue(null)
                }
            })
        }
    }


}