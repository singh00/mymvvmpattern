package com.example.mvvmex.repository

import android.content.Context
import com.example.mvvmex.network.ApiInterface

class UserRepository(private val retrofitService:ApiInterface,private var context: Context) {
    fun getUserDetails()=retrofitService.getUserDetails()
}