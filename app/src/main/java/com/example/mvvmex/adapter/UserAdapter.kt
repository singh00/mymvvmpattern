package com.example.mvvmex.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.mvvmex.R
import com.example.mvvmex.databinding.SingleItemViewBinding
import com.example.mvvmex.reaponsemodel.UserResponseModel

class UserAdapter():RecyclerView.Adapter<UserAdapter.MyViewHolder>(){

    var MyData=ArrayList<UserResponseModel.UserResponseModelItem>()

    fun setDataList(myData:ArrayList<UserResponseModel.UserResponseModelItem>){
        this.MyData=myData
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int)= MyViewHolder(DataBindingUtil.inflate(
        LayoutInflater.from(parent.context), R.layout.single_item_view,parent,false))

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.binding.useritemview= MyData[position]
    }

    override fun getItemCount(): Int {
       return MyData.size
    }


    class MyViewHolder(val binding: SingleItemViewBinding)
        :RecyclerView.ViewHolder(binding.root)

}