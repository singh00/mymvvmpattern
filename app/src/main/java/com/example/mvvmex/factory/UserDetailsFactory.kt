package com.example.mvvmex.factory

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.mvvmex.repository.UserRepository
import com.example.mvvmex.viewmodel.UserDetailsViewModel
import java.lang.IllegalArgumentException

class UserDetailsFactory(private val repository: UserRepository,private var context: Context):ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
       return if (modelClass.isAssignableFrom(UserDetailsViewModel::class.java)){
           UserDetailsViewModel(this.repository,context) as T
       }else{
           throw IllegalArgumentException("UserDetailsView not found")
       }
    }
}