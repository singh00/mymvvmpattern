package com.example.mvvmex.reaponsemodel

class UserResponseModel : ArrayList<UserResponseModel.UserResponseModelItem>(){
    data class UserResponseModelItem(
        val body: String,
        val email: String,
        val id: Int,
        val name: String,
        val postId: Int
    )
}